#pragma once

#include "doc.h"

#include <string>
#include <map>

namespace sample_docs {

extern std::string const DEFAULT_DOC;
extern std::map<std::string, doc::Document> const DOCUMENTS;

}
