#pragma once

namespace util {

template<typename T>
inline T distance(T a, T b) {
    return b - a;
}

}
